# FST CAN Protocol
JSON specification for can bus messages and helper tools.

## Install

	$ pip install fcp

## Usage
```
	$ fcp
    Usage: python -m fcp [OPTIONS] COMMAND [ARGS]...

      CLI utility for managment of FCP JSON files.

    Options:
      --version
      --help     Show this message and exit.

    Commands:
      generate
      json_to_fcp2
```

## Implementations

 * Rust: https://gitlab.com/joajfreitas/fcp-rust
 * C++: https://gitlab.com/joajfreitas/fcp-cpp
 * C Generator: https://gitlab.com/joajfreitas/fcp-cgen

## Documentation

See [readthedocs.io](https://fcp-core.readthedocs.io/en/latest/)
